class AddImdbIdToMovie < ActiveRecord::Migration
  def change
    add_column :movies, :imdbid, :string
  end
end

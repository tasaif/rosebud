class AddImdbIdToReview < ActiveRecord::Migration
  def change
    add_column :reviews, :imdbid, :string
  end
end

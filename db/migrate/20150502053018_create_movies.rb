class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :meta

      t.timestamps null: false
    end
  end
end

class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "must be an email")
    end
  end
end

class RatingValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    range = ENV['RATINGRANGE'].to_i
    return if range.nil?
    unless value <= range and value >= 0
      record.errors[attribute] << (options[:message] || "must be an integer 0-#{range}")
    end
  end
end

#id: integer, email: string, comment: text, rating: integer, created_at: datetime, updated_at: datetime, imdbid: string)
class Review < ActiveRecord::Base
  validates :email, presence: true, email: true
  validates :rating, presence: true, numericality: { only_integer: true }
  validates :imdbid, presence: true

  def movie_title(_imdbid = nil)
    imdbid ||= self.imdbid
    imdbid ||= _imdbid
    movie = Movie.where(imdbid: imdbid).first
    if movie.nil?
      meta = Hashie::Mash.new(OMDBGateway.gateway.find_by_id(imdbid).body)
      movie = Movie.create(imdbid: imdbid, meta: meta.to_json)
    else
      meta = Hashie::Mash.new(JSON.parse movie.meta)
    end
    meta.Title
  end
end

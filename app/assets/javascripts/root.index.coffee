$ ->
  return if ($('.root.index').length == 0)

  do_search = ->
    new_error = (message) ->
      "<div class='well'>#{message}</div>"

    generic_error = (jqXHR, textStatus, errorThrown) ->
      $results.html(new_error(errorThrown))

    meta_success = (data, textStatus, jqXHR) ->
      genre_cell = $("tr[data-imdbid='#{data.imdbID}'] .genre")
      genre_cell.html data.Genre
      genre_cell.removeClass 'populating'
      $datatable = $('table.dataTable')
      if $('.populating').length == 0 and not $datatable.hasClass 'initialized'
        $datatable
          .addClass 'initialized'
          .dataTable
            paging: false
            searching: false
            info: false
          .show()

    search_success = (data, textStatus, jqXHR) ->
      new_table = (results) ->
        retval = "
          <table class='dataTable' width='100%' hidden=true>
           <thead>
            <tr>
              <th>Title</th>
              <th>Release Date</th>
              <th>Genre</th>
            </tr>
            <tbody>
          "
        $.each results, ->
          retval = retval + "
            <tr data-imdbid='#{this.imdbID}'>
              <td>#{this.Title}</td>
              <td>#{this.Year}</td>
              <td class='populating genre'>&nbsp;</td>
            </tr>
          "
        retval = retval + "</tbody></table>"
      $results = $('.results')
      if data.Error
        $results.html(new_error(data.Error))
        return
      $results.html new_table(data.Search)
      $.each $results.find('.genre'), ->
        imdbid = $(this).closest('tr').data 'imdbid'
        window.rosebud.get_movie_data imdbid, meta_success, generic_error
        return true

    url = "http://www.omdbapi.com/?r=json&s='#{$('.search input').val()}'"
    $.ajax url,
      dataType: 'json'
      success: search_success
      error: generic_error

  $('.search button').on 'click', ->
    do_search()

  $('.search input').on 'keydown', (ev) ->
    RETURN = 13
    do_search() if (ev.keyCode == RETURN)

  $(document).on 'click', 'tbody tr', null, ->
    imdbid = $(this).data 'imdbid'
    window.location = "/reviews/#{imdbid}"

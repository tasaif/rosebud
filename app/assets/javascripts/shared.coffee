$ ->
  window.rosebud = {}
  window.rosebud.get_movie_data = (imdbid, success, error) ->
    url = "http://www.omdbapi.com/?r=json&i=#{imdbid}"
    $.ajax url,
      dataType: 'json'
      success: success
      error: error

  $('.raty').raty
    number: parseInt($('#rating-range').html())
    readOnly: ->
      $(this).hasClass 'disabled'
    score: ->
      $(this).attr 'data-score'
    click: (value) ->
      $('#review_rating').val value

class ApplicationController < ActionController::Base
  helper_method :getMovieMeta
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def getMovieMeta(imdbid)
    movie = Movie.where(imdbid: @imdbid).first
    if movie.nil?
      @meta = Hashie::Mash.new(OMDBGateway.gateway.find_by_id(@imdbid).body)
      movie = Movie.create(imdbid: @imdbid, meta: @meta.to_json)
    else
      @meta = Hashie::Mash.new(JSON.parse movie.meta)
    end
  end
end

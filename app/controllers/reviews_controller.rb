class ReviewsController < ApplicationController
    def index
      @reviews = Review.all
    end
    def show
      @imdbid = params['id']
      @reviews = Review.where(imdbid: @imdbid)
      getMovieMeta @imdbid
    end
    def new
      @review = Review.new
      @imdbid = params['id']
      getMovieMeta @imdbid
    end
    def create
      @review = Review.new(params.require(:review).permit(:email, :rating, :comment, :imdbid))
      @imdbid = params['review']['imdbid']
      if @review.save
        redirect_to action: 'show', id: @imdbid
      else
        render 'new'
      end
    end
end
